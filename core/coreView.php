<?php
//include_once(ROOT . DS . "core" . DS . "application.php");
class coreView extends coreApplication
{
    protected $_head,
        $_body,
        $_siteTitle,
        $_outputBuffer,
        $_layout = DEFAULT_LAYOUT;

    public function __construct()
    {

    }

    public function render($viewName)
    {
        $viewArray = explode('/', $viewName);
        $viewString = implode(DS, $viewArray);
        
        $tmpBasePath = ROOT . DS . 'app' . DS . 'views' . DS;
        $tmpPathToView = $tmpBasePath . $viewString . '.php';
        $tmpPathToLayout = $tmpBasePath . DS . 'layouts' . DS . $this->_layout . 'Layout.php';

        if (file_exists($tmpPathToView)) {
            include($tmpPathToView);
            include($tmpPathToLayout);
        } else {
            die('The view \"' . $viewName . '\" does not exist.');
        }
    }

    //basically a getter / setter
    public function content($type)
    {
        switch ($type) {
            case 'head':
                return $this->_head;
                //break;
            case 'body':
                return $this->_body;
                //break;     
            default:
                return false;
                //break;
        }
    }


    public function start($type)
    {
        $this->_outputBuffer = $type;
        ob_start();
    }

    public function end()
    {
        if ($this->_outputBuffer == 'head') {
            $this->_head = ob_get_clean();
        } elseif ($this->_outputBuffer == 'body') {
            $this->_body = ob_get_clean();
        } else {
            die('You first have to run the start method!');
        }
    }

    public function getSiteTitle()
    {
        if ($this->_siteTitle == '') {
            return SITE_TITLE;
        }
        return $this->_siteTitle;
    }

    public function setSiteTitle(string $title)
    {
        $this->_siteTitle = $title;
    }

    public function setLayout(string $path)
    {
        $this->_layout = $path;
    }
}
