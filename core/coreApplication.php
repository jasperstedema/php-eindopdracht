<?php

class coreApplication
{

//methods
    public function __construct()
    {
        $this->setReporting();
        $this->unregisterGlobals();
    }

    private function setReporting()
    {
        if (DEBUG) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        } else {
            error_reporting(E_ALL);
            ini_set('display_errors', 0);
            ini_set('log_errors', 1);
            ini_set('error_log', ROOT . DS . 'tmp' . DS . 'logs' . DS . 'errors.log');
        }

    }

    private function unregisterGlobals()
    {

        //this is the way everybody unregisters Globals, it is here for security reasons.
        if (ini_get('register_globals')) {
            $globalsArray = ['_SESSION', '_COOKIE', '_POST', '_GET', '_REQUEST', '_SERVER', '_ENV', '_FILES'];
            foreach($globalsArray as $i){
                foreach ($GLOBALS[$i] as $key => $value) {
                    if ($GLOBALS[$key] === $value) {
                        unset($GLOBALS[$key]);
                    }
                }
            }
        }
    }
}

?>