<?php 
//require_once(ROOT . DS . "core" . DS . "coreApplication.php");

class coreController extends coreApplication
{
    protected $controller, $action;
    public $view;

    public function __construct($controller, $action)
    {
        parent::__construct();
        $this->controller = $controller;
        $this->action = $action;
        $this->view = new coreView();
    }
    
    protected function loadModel($model)
    {
        if (class_exists($model)) {

            $modelTrim = strtolower(rtrim($model, 'Model'));
            
            $this->$model = new $model($modelTrim);
            //formatted_print_r($this->$model);
            //die();
        }
    }
}
