<?php

class coreCookie
{
    public static function set($name, $value,int $expiry)
    {
        //if (setCookie($name, $value, time() + $expiry, '/')) { //hier verder Warning: setcookie() expects parameter 3 to be integer, float given nadat /register/logout // ik denk dat het de uniqid gebeuren is
            return setCookie($name, $value, time() + $expiry, '/');

    }
    public static function delete($name)
    {
        //formatted_print_r($name);
        $expireTime = -86400;
        self::set($name, '', $expireTime);
    }

    public static function get($name)
    {
        return $_COOKIE[$name];
    }
    public static function exists($name)
    {
        return isset($_COOKIE[$name]);
    }
}
