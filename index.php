<?php 

//for debugging purposes
//formattedprint_r($_SERVER);

//set constants.l
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));

//load config and helper functions
require_once(ROOT . DS . 'config' . DS . 'config.php');
require_once(ROOT . DS . 'app' . DS . 'lib' . DS . 'helpers' . DS . 'functions.php');
require_once(ROOT . DS . 'core' . DS . 'coreRouter.php');

//autoload classes


function autoload($fullClassName)
{  
    //home Controller
    $classNameArray = camelCaseExplode($fullClassName, false, 'AB Cd');
    //$mvcName = $classNameArray[count($classNameArray) - 1];
    $mvcName = array_splice($classNameArray, -1, 1)[0];
    $className = implode($classNameArray);

    $mvcFolderName = strtolower($mvcName) . 's';

/*
    if (isset($classNameArray[1])) {
        $classNameSuffix = $classNameArray[1];
    }else {
        $classNameSuffix = '';
    }
     */

    //$pathToModelCore = ROOT . DS . 'app' . DS . 'models' . DS . 'modelCore.php';
    //echo ($pathToModelCore . '<br>');

    //$pathToViewCore = ROOT . DS . 'app' . DS . 'views' . DS . 'viewCore.php';
    //echo ($pathToViewCore . '<br>');

    //$controllers_className = ROOT . DS . 'core' . DS . $className . '.php';

    //$pathToCoreController = ROOT . DS . 'app' . DS . 'controllers' . DS . 'controllerCore.php';

    

    //$pathToClassController = ROOT . DS . 'app' . DS . 'controllers' . DS . $className . DS . $className . 'Controller.php';

    //$pathToClassModel = ROOT . DS . 'app' . DS . 'models' . DS . $className . DS . $className . 'Model.php';

    $pathToClass = ROOT . DS . 'app' . DS . $mvcFolderName . DS . $className . DS . $fullClassName . '.php';

    //$pathToClassModel = ROOT . DS . 'app' . DS . 'models' . DS . $className . DS . $className . 'Model.php';


    //$className = $className . $classNameSuffix;

    $pathToCore = ROOT . DS . 'core' . DS . $fullClassName . '.php';

    if (file_exists($pathToCore)) {
        require_once($pathToCore);
        //echo ($pathToCore . '<br>');

    } /*elseif (file_exists($pathToControllerCore)) {
        require_once($pathToControllerCore);
        echo ($pathToControllerCore . '<br>');

}*/ /*elseif (file_exists($pathToClassController)) {
        require_once($pathToClassController);
        echo ($pathToClassController . '<br>');

    }*/ elseif (file_exists($pathToClass)) {
        require_once($pathToClass);
        //echo ($pathToClass . '<br>');

    } /*elseif (file_exists($pathToClassModel)) {
        require_once($pathToClassModel);
        echo ($pathToClassModel . '<br>');
    }*/
}
spl_autoload_register('autoload');




// start session
session_start();

//for debugging purposes
//formattedprint_r($_SESSION);

//set url to url array
if (isset($_SERVER['PATH_INFO'])) {
    $url = explode('/', ltrim($_SERVER['PATH_INFO'], '/'));
} else {
    $url = [];
}

if (!coreSession::sessionExists(CURRENT_USER_SESSION_NAME) && coreCookie::exists(REMEMBER_ME_COOKIE_NAME)) {
    usersModel::loginUserFromCookie();
}
//formatted_print_r($db);
//route the requests
CoreRouter::route($url);
?>
