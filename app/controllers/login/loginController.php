<?php

class loginController extends coreController
{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        //set layout here
        //echo("Hello From ". __CLASS__);
    }

    public function indexAction()
    {
        die("Hello from: " . __class__ . ' and from: ' . __FUNCTION__);
    }

    public function helloAction()
    {
        die("Hello from: " . __class__ . ' and from: ' . __FUNCTION__);
    }
}
?>