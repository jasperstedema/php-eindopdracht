<?php
class registerController extends coreController
{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->loadModel('usersModel');
        $this->view->setLayout('default');
    }

    public function loginAction()
    {
        $validation = new coreValidate();
        if ($_POST) {
            //form validation
            $validation->check($_POST, [
                'username' => [
                    'display' => 'Username',
                    'required' => true
                ],
                'password' => [
                    'display' => 'Password',
                    'required' => true,
                    'min' => 6
                ]
            ]);
            if ($validation->passed()) {
                $user = $this->usersModel->findByUsername($_POST['username']);
                //formatted_print_r($user);
                if ($user && password_verify(coreInput::getInput('password'), $user->password)) {
                    if (isset($_POST['remember_me']) && coreInput::getInput('remember_me')) {
                        $user->login(true);
                    } else {
                        $user->login(false);
                    }
                    //formatted_print_r($user);
                    coreRouter::redirect('');
                } else {
                    $validation->addError("There is an error with your username or password.");
                }
            }

        }
        $this->view->displayErrors = $validation->displayErrors();
        $this->view->render('register/loginView');
    }

    public function logoutAction()
    {
        //formatted_print_r(currentUser());
        if (currentUser()) {
            currentUser()->logout();    
        }
        coreRouter::redirect('register/login');
    }
}
?>