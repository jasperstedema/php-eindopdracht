<?php 

class toolsController extends coreController
{
    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
        $this->view->setLayout('default');
    }

    public function indexAction()
    {
        $this->view->setLayout('tools/index');
    }

    public function firstAction()
    {
        $this->view->setLayout('tools/first');
    }

    public function secondAction()
    {
        $this->view->setLayout('tools/second');
    }

    public function thirdAction()
    {
        $this->view->setLayout('tools/third');
    }

}

?>