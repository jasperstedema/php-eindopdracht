<?php //set site title ?>
<?php $this->setSiteTitle('Home'); ?>

<?php //head ?>
<?php $this->start('head'); ?>
<meta content="test" />

<?php $this->end(); ?>

<?php //body ?>
<?php $this->start('body'); ?>
<h1 class="text-center">Hello World!</h1>
<div style="width:500px; margin:0 auto;">
<a href="<?= PROOT ?>register/logout" role="button" class="btn btn-primary">Logout</a>
<a href="<?= PROOT ?>cms/" role="button" class="btn btn-primary disabled">cms</a>
</div>
<?php $this->end(); ?>
