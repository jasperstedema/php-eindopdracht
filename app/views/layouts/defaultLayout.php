<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= PROOT ?>public/stylesheets/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="<?= PROOT ?>public/stylesheets/custom.css" media="screen" title="no title" charset="utf-8">

    <!-- Scripts -->
    <script src="<?= PROOT ?>public/javascripts/jquery-3.3.1.slim.min.js"></script>
    <script src="<?= PROOT ?>public/javascripts/bootstrap.min.js"></script>

    <?= $this->content('head'); ?>

    <title><?= $this->getSiteTitle(); ?></title>
  </head>
  <body>
    <?= $this->content('body'); ?>


  </body>
</html>