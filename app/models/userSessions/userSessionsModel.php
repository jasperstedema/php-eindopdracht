<?php 

class userSessionsModel extends coreModel
{
    public function __construct()
    {
        $table = 'User_Sessions';
        parent::__construct($table);
    }

    public static function getFromCookie()
    {
        $userSession = new self();
        if (coreCookie::exists(REMEMBER_ME_COOKIE_NAME)) {
        $userSession = $userSession->findFirstResult([
            'conditions' => "user_agent = ? AND session = ?",
            'bind' => [coreSession::userAgent_no_version(), coreCookie::get(REMEMBER_ME_COOKIE_NAME)]
        ]);
        }
        if (!$userSession) {
            return false;
        }
        return $userSession;
    }
}


