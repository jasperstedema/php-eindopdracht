<?php

class usersModel extends coreModel
{
    private $_isLoggedIn, $_sessionName, $_cookieName;
    public static $currentLoggedInUser = null;

    public function __construct($user = '')
    {
        $table = 'Users';
        parent::__construct($table);
        $this->_sessionName = CURRENT_USER_SESSION_NAME;
        $this->_cookieName = REMEMBER_ME_COOKIE_NAME;
        $this->_softDelete = true;
        if ($user != '') {
            if (is_int($user)) {
                $u = $this->_db->findFirstResult($table, ['conditions' => 'id = ?', 'bind' => [$user]]);
            } else {
                $u = $this->_db->findFirstResult($table, ['conditions' => 'userName = ?', 'bind' => [$user]]);
            }
            if ($u) {
                foreach ($u as $key => $val) {
                    $this->$key = $val;
                }
            }
        }
    }

    public function findByUsername($username)
    {
        return $this->findFirstResult(['conditions' => 'userName = ?', 'bind' => [$username]]);
    }

    public static function currentLoggedInUser()
    {
        if ((!isset(self::$currentLoggedInUser)) && (coreSession::sessionExists(CURRENT_USER_SESSION_NAME))) {
            $u = new usersModel((int)coreSession::getSession(CURRENT_USER_SESSION_NAME));
            self::$currentLoggedInUser = $u;
        }
        return self::$currentLoggedInUser;
    }


    public function login($rememberMe = false)
    {
        coreSession::setSession($this->_sessionName, $this->id);
        if ($rememberMe) {
            $hash = md5(uniqid() . rand(0, 100));
            $userAgent = coreSession::userAgent_no_version();
            coreCookie::set($this->_cookieName, $hash, REMEMBER_ME_COOKIE_EXPIRY);
            $fields = [
                'session' => $hash,
                'user_agent' => $userAgent,
                'userId' => $this->id
            ];
            $this->_db->query("DELETE FROM User_Sessions WHERE userId = ? AND user_agent = ?", [$this->id, $userAgent]);
            $this->_db->insert('User_Sessions', $fields);
        }
    }

    public static function loginUserFromCookie()
    {
        $userSession = userSessionsModel::getFromCookie(); 
        if ($userSession->userId != '') {
            $user = new self((int)$userSession->userId);
            $user->login();
            return $user; //self::$currentLoggedInUser;
        }
    }

    public function logout()
    {
        //delete stored cookie id from server
        $userSession = userSessionsModel::getFromCookie();
        if ($userSession) {
            $foo = $userSession->deleteByID($userSession->id);
        }

        //delete local session
        coreSession::deleteSession(CURRENT_USER_SESSION_NAME);
       
        //delete local cookie
        if (coreCookie::exists(REMEMBER_ME_COOKIE_NAME)) {
            coreCookie::delete(REMEMBER_ME_COOKIE_NAME);
        }

        //set current logged in user to null
        self::$currentLoggedInUser = null;
        return true;
    }




}


